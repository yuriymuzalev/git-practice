package com.epam.stud;

public class First {

    public static final String CLASS_HAS_BEEN_CHANGED = "Class has been changed by origin and clone";

    public static void main(String[] args) {
        System.out.println(CLASS_HAS_BEEN_CHANGED);
    }
}
